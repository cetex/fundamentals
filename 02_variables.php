<?php
// Kintamuju tipai
/*
    String
    Integer
    Float
    Boolean
    Null
    Array
    Object
    Resource
*/

// Sukuriam kintamuosius su skirtingais tipais
$name = "Zura";     // String
$age = 28;          // Integer
$isMale = true;     // Boolean
$height = 1.85;     // Float
$salary = null;     // Null

// Isvedam i ekrana kintamuosius
echo $name . PHP_EOL;
echo $age . PHP_EOL;
echo $isMale . PHP_EOL;
echo $height . PHP_EOL;
echo $salary . PHP_EOL;

// Isvedam kintamuju tipus
echo gettype($name) . PHP_EOL;
echo gettype($age) . PHP_EOL;
echo gettype($isMale) . PHP_EOL;
echo gettype($height) . PHP_EOL;
echo gettype($salary) . PHP_EOL;

// Pažiūrim kintamūjų tipą ir reikšmę
var_dump($name) . PHP_EOL;
var_dump($age) . PHP_EOL;
var_dump($isMale) . PHP_EOL;
var_dump($height) . PHP_EOL;
var_dump($salary) . PHP_EOL;

// Perrašom kintamojo reikšme
$name = false;          // Nustatom naują reikšmę
echo gettype($name);    // Gaunam kintamojo tipą

/*
 * Kintamuju tikrinimo funkcijos
 *
 * is_string()                  - tikrinam ar perduoto parametro reikšmė yra String | Grąžina TRUE arba False
 * is_int()                     - tikrinam ar perduoto parametro reikšmė yra Integer | Grąžina TRUE arba False
 * is_bool()                    - tikrinam ar perduoto parametro reikšmė yra Boolean | Grąžina TRUE arba False
 * is_double() | is_float()     - tikrinam ar perduoto parametro reikšmė yra Float | Grąžina TRUE arba False
*/

//Peržiūrim funkcijų gatutus atsakymus, perdavus kintamuosius kaip parametrus
var_dump(is_string($name));
var_dump(is_int($age));
var_dump(is_bool($isMale));
var_dump(is_double($height));

// Pažiūrim ar kintamieji sukurti
var_dump(isset($name)); // TRUE
var_dump(isset($name2)); //FALSE

//Konstantas galima rašyti dviem būdais:
//define('PI', 3.14); // 1. būdas
const PI2 = 3.14; // 2.būdas

//Konstantų išvedimas į ekraną
echo PI . PHP_EOL;
echo PI2 . PHP_EOL;

// defined() - funckija skirta tikrinti ar konstanta buvo sukurta | Grąžina TRUE arba False
var_dump(defined('PI'));

// PHP konstantos kurios automatiškai egzistuoja
echo SORT_ASC . PHP_EOL;
echo PHP_INT_MAX . PHP_EOL;
echo __DIR__ . PHP_EOL;
echo __FILE__ . PHP_EOL;

//https://www.php.net/manual/en/language.variables.variable.php