<?php
// Sukuriam kintamajį kuris yra tuščias masyvas
$fruits = [];
var_dump($fruits);

// Sukuriam masyvą su duomenimis ir automatiškai sugeneruotais raktais
$fruits = ['Banana', 'Apple', 'Orange'];
// arba galima rašyti ir taip:
//$fruits = [
//    'Banana',
//    'Apple',
//    'Orange'
//];
var_dump($fruits);

// Sukuriam masyvą su duomenimis ir rankiniu būdu nustatytais raktais
$fruits = ['nulis' => 'Banana', 'vienas' => 'Apple', 'du' => 'Orange'];
// arba galima rašyti ir taip:
//$fruits = [
//    'nulis' => 'Banana',
//    'vienas' => 'Apple',
//    'du' => 'Orange'
//];
var_dump($fruits);

// Sukuriam masyvą su masyve su atomatiškai sugeneruotais raktais:
$foods = [
    [
        'Banana',
        'Apple',
        'Orange',
    ],
];

// Sukuriam masyvą su masyve su rankiniu būdu sugeneruotais raktais:
$foods = [
    'fruits' => [
        'nulis' => 'Banana',
        'vienas' => 'Apple',
        'du' => 'Orange',
    ],
];

// Sukuriam masyvą kuriame yra keli masyvai (masyve masyvų skaičius nėra ribojamas, taip ir kiek masyvu gali buti masyvuose)
$foods = [
    'fruits' => [
        'Banana',
        'Apple',
        'Orange',
    ],
    'vegetables' => [
        'Potato',
        'Cucumber',
    ],
];


// Masyvu isvedimas i ekrana
echo ' < pre>';
var_dump($fruits); // Grazina su types, keys ir values
print_r($fruits);  // Grazina su keys ir values
echo ' </pre > ';


// Gaunam masyvo elemento reiksme
echo $fruits[1] . PHP_EOL;

// Gaunam masyvo elemento reiksme
echo $foods['fruits'][0] . PHP_EOL; // Apple
echo $foods['vegetables'][2] . PHP_EOL; // Cucumber



// Tikrinam ar egzistuoja toks masyvo key
echo '<pre > ';
var_dump(isset($fruits[2]));
echo '</pre > ';

// Skaiciuojam kiek masyve elementu
echo count($fruits) . PHP_EOL;

// Add element at the end of the array
$fruits[] = 'Peach';
echo $fruits[3] . PHP_EOL;
array_push($fruits, 'Foo');
// Remove element from the end of the array
array_pop($fruits);

echo ' < pre>';
var_dump($fruits);
echo ' </pre > ';

// Add element at the beginning of the array
array_unshift($fruits, 'Apple');
// Remove element from the beginning of the array
array_shift($fruits);

// Split the string into an array
$string = "Banana,Apple,Peach";
echo ' < pre>';
var_dump(explode(",", $string));
echo ' </pre > ';

print_r($fruits);

// Combine array elements into string
echo implode(",", $fruits) . PHP_EOL;

// Check if element exist in the array
echo '<pre > ';
var_dump(in_array('Apple', $fruits));
echo ' </pre > ';

// Search element index in the array
echo '<pre > ';
var_dump(array_search("Peach", $fruits));
echo '</pre > ';

// Merge two arrays
$vegetables = ['Potato', 'Cucumber'];
echo ' < pre>';
var_dump(array_merge($fruits, $vegetables));
var_dump([...$fruits, ...$vegetables]); // Since PHP 7.4
echo ' </pre > ';

// Sorting of array (Reverse order also)
sort($fruits); //sort, rsort
echo '<pre > ';
var_dump($fruits);
echo '</pre > ';


// https://www.php.net/manual/en/ref.array.php

// ============================================
// Associative arrays
// ============================================

// Create an associative array
$person = [
    'name' => 'Brad',
    'surname' => 'Traversy',
    'age' => 30,
    'hobbies' => ['Tennis', 'Video Games'],
];
// Get element by key
echo $person['name'] . PHP_EOL;

// Set element by key
$person['channel'] = 'TraversyMedia';

//Null coalescing assignment operator. Since PHP 7.4
if (!isset($person['address'])) {
    $person['address'] = 'Unknown';
}
$person['address'] ??= 'Unknown';
echo $person['address'] . PHP_EOL;

// Check if array has specific key
echo ' < pre>';
var_dump(isset($person['age']));  // Change age into "location"
echo ' </pre > ';

// Print the keys of the array
echo '<pre > ';
var_dump(array_keys($person));
echo '</pre > ';

// Print the values of the array
echo '<pre > ';
var_dump(array_values($person));
echo '</pre > ';

// Sorting associative arrays by values, by keys
ksort($person); // ksort, krsort, asort, arsort
echo '<pre > ';
var_dump($person);
echo '</pre > ';