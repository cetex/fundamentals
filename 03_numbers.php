<?php

// Sukuriam kintamuosius
$a = 5;
$b = 4;
$c = 1.2;

// Aritmetinės operacijos
echo ($a + $b) * $c . PHP_EOL;
echo $a - $b . PHP_EOL;
echo $a * $b . PHP_EOL;
echo $a / $b . PHP_EOL;
echo $a % $b . PHP_EOL;

// Assignment with math operators

//$a += $b; echo $a.PHP_EOL; // $a = 9
//$a -= $b; echo $a.PHP_EOL; // $a = 1
//$a *= $b; echo $a.PHP_EOL; // $a = 20
//$a /= $b; echo $a.PHP_EOL; // $a = 1.25
//$a %= $b; echo $a.PHP_EOL; // $a = 1

// Didinimo operatoriai
echo $a++ . PHP_EOL;
echo ++$a . PHP_EOL;

// Decrement operator
echo $a-- . PHP_EOL;
echo --$a . PHP_EOL;

// Number checking functions
is_float(1.25); // true
is_integer(3.4); // false
is_numeric("3.45"); // true
is_numeric("3g.45"); // false

// Conversion
$strNumber = '12.34';
$number = (float)$strNumber; // Use floatval(), (int), intval()
var_dump($number);
echo PHP_EOL;

// Number functions
echo "abs(-15) " . abs(-15) . PHP_EOL;
echo "pow(2,  3) " . pow(2, 3) . PHP_EOL;
echo "sqrt(16) " . sqrt(16) . PHP_EOL;
echo "max(2, 3) " . max(2, 3) . PHP_EOL;
echo "min(2, 3) " . min(2, 3) . PHP_EOL;
echo "round(2.4) " . round(2.4) . PHP_EOL;
echo "round(2.6) " . round(2.6) . PHP_EOL;
echo "floor(2.6) " . floor(2.6) . PHP_EOL;
echo "ceil(2.4) " . ceil(2.4) . PHP_EOL;

// Formatting numbers
$number = 123456789.12345;
echo number_format($number, 2, '.', ',') . PHP_EOL;

// https://www.php.net/manual/en/ref.math.php