<?php

$age = 20;
$salary = 300000;

// Sample if
//if ($age == 21) {
//    echo "1".PHP_EOL;
//}

// Without circle braces
//if ($age === 20)
//    echo "1";

// Sample if-else
if ($age == 20) {
    echo "1";
} else if ($age == 14) {
    echo "1";
} else if ($age == 10) {
    echo "1";
} else {
    echo "3";
}

if(in_array($age, [20,14,10])){
    echo "1";
}else{
    echo "3";
}

//
//// Difference between == and ===
//$age == 20; // true
//$age == '20'; // true
//
//$age === 20; // true
//$age === '20'; // false
//
//// if AND
//if ($age > 20 && ($salary === 300000 or $salary === 400000)) {
//
//}
//// if OR
//if ($age > 20 or $salary === 300000) {
//
//}

// Ternary if
//echo isset($age) ? strtoupper('aaaa') : 'nera';
//
//exit;
//echo '<br>';

// Short ternary
//$age = null;
//$myAge = $age ?: 18; // Equivalent of "$age ? $age : 18"
//
//var_dump($myAge);
//exit;
// Null coalescing operator
//$var = isset($name) ? $name : 'Johna';
//$var = $name ?? 'John'; // Equivalent of above
////echo $name.PHP_EOL;
//echo $var.PHP_EOL;
//
//exit;

// switch
$userRole = 'ateivis'; // admin, editor, user
//
switch ($userRole) {
    case 'admin':
    case 'editor':
        echo 'You can do anything<br>';
        break;
    case 'user':
        echo 'You can view posts and comment<br>';
        break;
    default:
        echo 'Unknown role<br>';
}



//if('vardas' == 'petras' && 'false' == true){}
//if('vardas' == 'petras' and 'false' == true){}

//if('18' < 14 || '18' > 60){}
//if('18' < 14 or '18' > 60){}

//if ('user' == 'user') && ('a' != 'b' || 't' == 'c')){}