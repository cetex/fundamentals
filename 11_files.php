<?php

// Magic constants
echo __FILE__ . PHP_EOL;
echo __DIR__ . PHP_EOL;




// Create directory
//mkdir(__DIR__. '/test/abc/a');


// Rename directory
//rename('test', 'test2');


// Delete directory
//rmdir('test2/abc/a');


// Read files and folders inside directory
//$files = scandir('blocks');
//echo '<pre>';
//print_r($files);
//echo '</pre>';
//exit;




// file_get_contents, file_put_contents
//$text = file_get_contents('11_files/text.txt');
//echo $text;
//file_put_contents('11_files/text.txt', $text . rand(0,10));
//echo PHP_EOL;
//echo $text;
//exit;
//echo '<br>';
//file_put_contents('lorem.txt', "First line" . PHP_EOL . $lorem);



// file_get_contents from URL
//$jsonContent = file_get_contents('https://jsonplaceholder.typicode.com/users');
//$users = json_decode($jsonContent);
//var_dump($users);
//exit;



// Check if file exists or not
//file_exists('lorem.txt'); // true

// Get file size
var_dump(filesize('11_files/text.txt'));

// Delete file
//unlink('lorem.txt');