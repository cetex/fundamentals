<?php


// Function which prints "Hello I am Zura"
//function hello()
//{
//    echo 'Hello I am Zura<br>';
//}
//
//hello();
//hello();
//
//exit;

/**
 * @param string $name
 * @param int $age
 * @return void
 */
function hello(string $name, int $age)
{
    echo "Hello I am $name. My age: $age";
}
//
//
//
//
//hello('sarunas', 'aa');
//
//exit;

/**
 * @param int $a
 * @param int $b
 * @return int
 */
//function sum(int $a, int $b): int
//{
//    return $a + $b;
//}
//
//
//
//
//'20'
//20
//
//
//
//echo sum(4, 1).PHP_EOL;
//echo sum(9, 10).PHP_EOL;
//
//exit;


// Create function to sum all numbers using ...$nums
/**
 * @param array ...$numbers
 * @return array|int
 */
function sum(array $numbers)
{
    $sum = 0;
    foreach ($numbers as $number){
        $sum += $number;
    }

    return $sum;
}
//echo sum(1, 2, 3, 4, 6);
echo sum([1, 2, 3, 4, 6]);
exit;


// Arrow functions
//function sum(...$nums)
//{
//    return array_reduce($nums, fn($carry, $n) => carry + $n);
//}
//echo sum(1, 2, 3, 4, 6);